var getDdeliveryModule = (function () {
    $.fn.exists = function () {
        return this.length !== 0;
    }
    function debounce(f, ms) {
        var timer = null;

        return function (data) {
            var _this = this;

            function onComplete() {
                f.call(_this, data);
                timer = null;
            }

            if (timer) clearTimeout(timer);

            timer = setTimeout(onComplete, ms);
        };
    }

    var widget;

    return function () {
        $('label[for="ddelivery.ddelivery"]').after('<div id="dd-widget"></div>');
        $('input').filter(function () {
            return this.id.match(/shipping_field\d+/);
        }).hide();
        $('label').filter(function () {
            return this.htmlFor.match(/shipping_field\d+/);
        }).remove();
        $('.radio').css('margin-bottom', 0);

        if (widget) widget.destruct();


        var onChange = debounce(function (data) {
            $.ajax({
                method: 'POST',
                url: '/index.php?route=module/ddelivery/savedata',
                data: {data: data, type: 'change'}
            }).done(function (result) {
                if (data.delivery.total_price == undefined) {
                    data.delivery.total_price = 0;
                }
                if (data.delivery.point != undefined) {
                    data.delivery.total_price = data.delivery.point.price_delivery;
                }
                $('#total_shipping > .simplecheckout-cart-total-value').html(
                    data.delivery.total_price.toFixed(2) + 'р.'
                );
                $('#total_total > .simplecheckout-cart-total-value').html(
                    (parseInt($('#total_sub_total > .simplecheckout-cart-total-value').html()) + data.delivery.total_price).toFixed(2) + 'р.'
                );
                if (result.status == 'ok') {
                    $('input').filter(function () {
                        return this.id.match(/shipping_field\d+/);
                    }).val('ok');
                }
            });
        }, 1000);


        $.ajax('/index.php?route=module/ddelivery/check_process').done(function (response) {

            $.ajax('/index.php?route=module/ddelivery/getcart').done(function (products) {
                if (products.products.length != 0) {
                    widget = new DDeliveryWidgetCart('dd-widget', {
                        apiScript: '/index.php?route=module/ddelivery/process',
                        products: products.products,
                        width: products.width,
                        height: products.height,
                        length: products.length,
                        weight: products.weight,
                    });

                    widget.on('change', onChange);

                    widget.on('afterSubmit', function (response) {
                        $.ajax({
                            method: 'POST',
                            url: '/index.php?route=module/ddelivery/savedata',
                            data: {data: response, type: 'afterSubmit'}
                        }).done(function (result) {
                            if (result.status == 'ok') {
                                $('input').filter(function () {
                                    return this.id.match(/shipping_field\d+/);
                                }).val('ok');
                            }
                        });
                    });

                    widget.on('error', function (e) {
                        console.error(e);
                    });
                }
            });
        });
    }
})();