<?php
use DDelivery\Adapter\Adapter;
use DDelivery\DDeliveryException;

/**
 * Created by PhpStorm.
 * User: mrozk
 * Date: 4/11/15
 * Time: 2:53 PM
 */
class OcAdapter extends Adapter
{
    /**
     *
     * Получить апи ключ
     *
     * @throws DDeliveryException
     * @return string
     */
    public function getApiKey()
    {
        return $this->token;
    }

    /**
     * Настройки базы данных
     * @return array
     */
    public function getDbConfig()
    {

        return array(
            'type' => self::DB_MYSQL,
            'dsn' => 'mysql:host=' . DB_HOSTNAME . ';dbname=' . DB_DATABASE,
            'user' => DB_USERNAME,
            'pass' => DB_PASSWORD,
            'prefix' => '',
        );

    }

    /**
     *
     * При синхронизации статусов заказов необходимо
     * array(
     *      'id' => 'status',
     *      'id2' => 'status2',
     * )
     *
     * @param array $orders
     * @return bool
     */
    public function changeStatus(array $orders)
    {
        // TODO: Implement changeStatus() method.
    }

    /**
     * Получить урл апи сервера
     *
     * @return string
     */
    public function getSdkServer()
    {
        return 'https://sdk.ddelivery.ru/api/v1/';
    }

    public function getCmsName()
    {
        return 'Open Cart';
    }

    public function getCmsVersion()
    {
        return '2.3.x';
    }

    public function getEnterPoint()
    {
        return "http://" . $_SERVER['HTTP_HOST'] . "/index.php?route=module/ddelivery/ddeliveryEndpoint/";
    }

    /**
     * Получить  заказ по id
     * ['city' => город назначения, 'payment' => тип оплаты, 'status' => статус заказа,
     * 'sum' => сумма заказа, 'delivery' => стоимость доставки]
     *
     * город назначения, тип оплаты, сумма заказа, стоимость доставки
     *
     * @param $id
     * @return array
     */
    public function getOrder($id)
    {
        return [];
    }

    /**
     * Получить список заказов за период
     * ['city' => город назначения, 'payment' => тип оплаты, 'status' => 'статус заказа'
     * 'sum' => сумма заказа, 'delivery' => стоимость доставки]
     *
     * город назначения, тип оплаты, сумма заказа, стоимость доставки
     *
     * @param $from
     * @param $to
     * @return array
     */
    public function getOrders($from, $to)
    {
        return [];
    }

    /**
     * Получить скидку в рублях
     *
     * @return float
     */
    public function getDiscount()
    {
        return 0;
    }

    /**
     *
     * Получить содержимое корзины
     *
     * @return array
     */
    public function getProductCart()
    {
        return $this->params['form'];
    }

    /**
     * Получить массив с соответствием статусов DDelivery
     * @return array
     */
    public function getCmsOrderStatusList()
    {
        return array();
    }

    /**
     * Получить массив со способами оплаты
     * @return array
     */
    public function getCmsPaymentList()
    {
        return [];
    }

    /***
     *
     * В этом участке средствами Cms проверить права доступа текущего пользователя,
     * это важно так как на базе этого  метода происходит вход
     * на серверние настройки
     *
     * @return bool
     */
    public function isAdmin()
    {
        return true;
    }

    public static function stringToNumber($string)
    {
        $alphabet = array(
            'a' => '01',
            'b' => '02',
            'c' => '03',
            'd' => '04',
            'e' => '05',
            'f' => '06',
            'g' => '07',
            'h' => '08',
            'i' => '09',
            'j' => '10',
            'k' => '11',
            'l' => '12',
            'm' => '13',
            'n' => '14',
            'o' => '15',
            'p' => '16',
            'q' => '17',
            'r' => '18',
            's' => '19',
            't' => '20',
            'u' => '21',
            'v' => '22',
            'w' => '23',
            'x' => '24',
            'y' => '25',
            'z' => '26'
        );
        $string = trim((string)$string);
        $string = strtolower($string);
        $len = strlen($string);
        $rez = '';
        for ($i = 0; $i < $len; $i++) {
            if (isset($alphabet[$string[$i]])) {
                $rez .= $alphabet[$string[$i]];
            }
        }
        $rez = '1' . $rez;
        if (strlen($rez) > 32) {
            $rez = substr($rez, 0, 32);
        }

        return (int)$rez;
    }

}