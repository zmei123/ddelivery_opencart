<?php
use DDelivery\Adapter\Container;
use DDelivery\Business\Business;

require('ddelivery/ddelivery_helper.php');

class ModelShippingDdelivery extends Model
{

    const DDELIVERY_API_ULR = 'https://ddelivery.ru/api/';

    /**
     * Создание заказа(отправка данных в DDelivery)
     * @param $order_id - id заказа в CMS
     * @return bool
     */
    function sendOrder($order_id)
    {
        $this->load->model('checkout/order');
        $this->load->model('catalog/product');
        $order = $this->model_checkout_order->getOrder($order_id);

        $this->load->model('account/order');
        if (!isset($this->session->data['ddelivery_afterSubmit'])) {
            return false;
        }
        $afterSubmit = json_decode(urldecode($this->session->data['ddelivery_afterSubmit']), true);

        $params = [
            'id' => $afterSubmit['data']['id'],
            'status' => $order['order_status_id'],
            'cms_id' => $order['order_id'],
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL,
                self::DDELIVERY_API_ULR . $this->config->get('ddelivery_api_key') . '/sdk/update-order.json');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            $out = curl_exec($curl);
            $mout = json_decode($out, true);
            $this->db->query('UPDATE `' . DB_PREFIX . 'order` SET ddelivery_id=\'' . $mout['data']['cabinet_id'] . '\',ddelivery_temp_id=\'' . addslashes($afterSubmit['data']['id']) . '\' WHERE order_id=' . $order_id);
            curl_close($curl);
        }
        $this->session->data['ddelivery_afterSubmit'] = '';

        return true;
    }

    /**
     * Редактирование заказа(отправка статусов в DDelivery)
     * @param $order_id - id заказа в CMS
     * @param $order_status_id - id статуса заказа в CMS
     */
    function editOrder($order_id, $order_status_id)
    {
        $this->load->model('checkout/order');
        $this->load->model('catalog/product');
        $this->load->model('account/order');
        $order_data = $this->db->query('SELECT * FROM `' . DB_PREFIX . 'order` WHERE order_id=' . $order_id);

        $order_status = $this->db->query('SELECT * FROM `' . DB_PREFIX . 'order_status` WHERE order_status_id=' . $order_status_id);

        $params = [
            'id' => $order_data->row['ddelivery_temp_id'],
            'status' => $order_status_id,
            'cms_id' => $order_id,
            'payment_method' => OcAdapter::stringToNumber($order_data->row['payment_code']),
        ];
        if ($order_status->row['name'] == 'Оплачено') {
            $params['payment'] = 'true';
        }

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL,
                self::DDELIVERY_API_ULR . $this->config->get('ddelivery_api_key') . '/sdk/update-order.json');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
            $out = curl_exec($curl);
            curl_close($curl);
            $mout = json_decode($out, true);
            if (isset($mout['data']) && isset($mout['data']['cabinet_id'])) {
                $this->db->query('UPDATE `' . DB_PREFIX . 'order` SET ddelivery_id=\'' . $mout['data']['cabinet_id'] . '\' WHERE order_id=' . $order_id);

                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Получение информации о доставке в корзине
     * @param $address
     * @return array
     */
    function getQuote($address)
    {
        $this->load->language('shipping/ddelivery');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('ddelivery_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if (!$this->config->get('ddelivery_geo_zone_id') || $query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = [];

        if ($status) {
            $quote_data = [];

            $ddelivery_price = (isset($this->session->data['ddelivery_price'])) ? $this->session->data['ddelivery_price'] : 0.0;

            $quote_data['ddelivery'] = [
                'code' => 'ddelivery.ddelivery',
                'title' => $this->language->get('text_description'),
                'cost' => $ddelivery_price,
                'tax_class_id' => 0,
                'ddelivery' => 'true',
                'text' => ''
            ];

            $method_data = [
                'code' => 'ddelivery',
                'title' => $this->language->get('text_title'),
                'quote' => $quote_data,
                'sort_order' => $this->config->get('ddelivery_sort_order'),
                'error' => false
            ];
        }

        return $method_data;
    }

    /**
     * Получение списка возможных статусов заказа
     * @return mixed
     */
    function getOrderStatuses()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "order_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";
        $sql .= " ORDER BY name";

        $query = $this->db->query($sql);

        foreach ($query->rows as $status) {
            $out[$status['order_status_id']] = $status['name'];
        }

        return $out;
    }
}