<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/catalog/model/shipping/DDeliveryWidgetApi.php";

class ControllerModuleDdelivery extends Controller
{

    /**
     * Получение токена для SDK
     */
    public function getToken()
    {

        $this->load->model('shipping/ddelivery');

        $token = $this->model_shipping_ddelivery->createSDK();

        $this->response->setOutput($token);
    }

    /**
     * Проверка отправки данных после заполнения виджета
     */
    public function check_process()
    {
        $this->response->addHeader('Content-Type: application/json');

        try {
            if (isset($this->session->data['ddelivery_afterSubmit'])) {
                $data = json_decode($this->session->data['ddelivery_afterSubmit'], true);
                if (count($data) == 0) {
                    $this->response->setOutput(json_encode(['status' => 'fail']));
                } else {
                    $this->response->setOutput(json_encode(['status' => true]));
                }
            } else {
                $this->response->setOutput(json_encode(['status' => 'fail']));
            }
        } catch (Exception $e) {
            $this->response->setOutput(json_encode(['status' => 'fail']));
        }
    }

    /**
     * Обновление статусов через API
     */
    public function update_status()
    {
        $this->load->language('api/order');
        $this->db->query('UPDATE `' . DB_PREFIX . 'order` SET order_status_id=' . intval($this->request->get['status']) . ',ddelivery_track_number=\'' . addslashes($this->request->get['ddelivery_track_number']) . '\' WHERE ddelivery_id=' . intval($this->request->get['ddelivery_id']));
        $order = $this->db->query('SELECT * FROM `' . DB_PREFIX . 'order` WHERE ddelivery_id=' . intval($this->request->get['ddelivery_id']));
        $order = $order->rows[0];

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($order['order_id']);
        if ($order_info) {
            $this->model_checkout_order->addOrderHistory($order['order_id'], intval($this->request->get['status']), '',
                false, false);
        }
    }

    /**
     * Функция взаимодействия с API кабинета.
     */
    public function process()
    {
        $widgetApi = new DDeliveryWidgetApi();

        // Передайте здесь свой API-key
        $widgetApi->setApiKey($this->config->get('ddelivery_api_key'));

        $widgetApi->setMethod($_SERVER['REQUEST_METHOD']);
        $widgetApi->setData(isset($_REQUEST['data']) ? $_REQUEST['data'] : []);

        echo $widgetApi->submit($_REQUEST['url']);

    }

    /**
     * Сохранение введенных данных в виджете
     */
    public function saveData()
    {
        $this->response->addHeader('Content-Type: application/json');
        if ($this->request->post['type'] == 'change') {
            $this->session->data['ddelivery_change'] = json_encode($this->request->post);
        }
        if ($this->request->post['type'] == 'afterSubmit') {
            $this->session->data['ddelivery_afterSubmit'] = json_encode($this->request->post);
            $this->response->setOutput(json_encode(['status' => 'ok']));
        } else {
            $this->response->setOutput(json_encode(['status' => 'fail']));
        }
    }

    /**
     * Получение коризны товаров для виджета
     */
    public function getCart()
    {
        $this->response->addHeader('Content-Type: application/json');
        $out = [];
        $width = 0;
        $length = 0;
        $height = 0;
        $weight = 0;
        foreach ($this->cart->getProducts() as $product) {
            $out[] = [
                'name' => $product['name'],
                'vendorCode' => $product['cart_id'],
                'barcode' => $product['product_id'],
                'price' => $product['price'],
                'count' => $product['quantity'],
            ];
            $width += (isset($product['width']) ? $product['width'] : 0);
            $length += (isset($product['length']) ? $product['length'] : 0);
            $height += (isset($product['height']) ? $product['height'] : 0);
            $weight += (isset($product['weight']) ? $product['weight'] : 0);
        }
        $this->response->setOutput(json_encode([
            'products' => $out,
            'width' => $width,
            'length' => $length,
            'height' => $height,
            'weight' => $weight
        ]));
    }

    /**
     * Список статусов для настоек магазина
     */
    public function getStatuses()
    {
        $this->response->addHeader('Content-Type: application/json');

        $this->load->model('shipping/ddelivery');
        $statuses = $this->model_shipping_ddelivery->getOrderStatuses();

        $this->response->setOutput($statuses);
    }

    /**
     * Список способов оплат для настроек магазина
     */
    public function getPayment()
    {
        $this->response->addHeader('Content-Type: application/json');

        $this->load->model('extension/extension');
        $payments = $this->model_extension_extension->getExtensions('payment');

        foreach ($payments as $payment) {
            $this->load->language('extension/payment/' . $payment['code']);
            $lang = $this->language->get('text_title');
            $out[$payment['extension_id']] = $lang;
        }
        $this->response->setOutput(json_encode($out));
    }

}

?>